import numpy as np
import json

from PB import richardson

mol_name = 'arg'
log_file = 'log.json'

with open('log.json') as infile:
    log = json.load(infile)

import matplotlib.pyplot as plt
plt.switch_backend('agg')

params = {'figure.figsize':  (7, 5),
		  'axes.titlesize':  12,
          'axes.labelsize':  12,
          'xtick.labelsize': 12,
          'ytick.labelsize': 12,
          'font.family': 'serif',
		  'legend.fontsize': 12,
		  'font.size': 14,
		  'lines.linewidth': .5,
		  'lines.color': 'k',
		  'legend.loc': 'lower right'}
plt.rcParams.update(params)

n_boundary_yoonlh = [run['config']['number_of_elements'] for run in log 
                    if run['config']['formulation'] == 'yoon_lenhoff' 
                    and run['config']['mol_name'] == mol_name]

n_boundary_juffer = [run['config']['number_of_elements'] for run in log 
                    if run['config']['formulation'] == 'juffer' 
                    and run['config']['mol_name'] == mol_name]

energy_yoon = [run['results']['solvarion_energy'] for run in log 
                if run['config']['formulation'] == 'yoon_lenhoff'][:-2]

energy_juff = [run['results']['solvarion_energy'] for run in log 
                if run['config']['formulation'] == 'juffer']

rich_energy_yoon, r_y, p_y = richardson.extrapolation(energy_yoon, n_boundary_yoonlh)
rich_energy_juff, r_j, p_j = richardson.extrapolation(energy_juff, n_boundary_juffer)

print('yoon: ',rich_energy_yoon, r_y, p_y)
print('juff: ',rich_energy_juff, r_j, p_j)

energy_rslt = plt.figure().add_subplot(111)
#energy_rslt.loglog(n_boundary_yoonlh, np.abs(energy_yoon), marker='o', label='yoonlh', color='k')
#energy_rslt.loglog(n_boundary_juffer, np.abs(energy_juff), marker='s', label='Juffer', color='k')

energy_rslt.plot(n_boundary_yoonlh, energy_yoon, marker='o', label='YL', color='k')
energy_rslt.plot(n_boundary_juffer, energy_juff, marker='s', label='Juffer', color='k')

r_solution_d = rich_energy_yoon*np.ones(len(n_boundary_yoonlh))
r_solution_j = rich_energy_juff*np.ones(len(n_boundary_juffer))

energy_rslt.plot(n_boundary_yoonlh, r_solution_d, 'r--', color='k')
energy_rslt.plot(n_boundary_juffer, r_solution_j, 'r--', color='k')

#energy_rslt.set_title('Mesh Convergence')
energy_rslt.set_xlabel('N of Elements')
energy_rslt.set_ylabel(r'$\Delta$G  [kcal/mol]')
energy_rslt.legend()
plt.savefig('IMG/Energy.png')


iterations_yoonlh = [l['results']['iterations'] for l in log 
                    if l['config']['formulation'] == 'yoon_lenhoff' 
                    and l['config']['mol_name'] == mol_name]

iterations_juffer = [l['results']['iterations'] for l in log 
                    if l['config']['formulation'] == 'juffer' 
                    and l['config']['mol_name'] == mol_name]

nmbr_it = plt.figure().add_subplot(111)

nmbr_it.plot(n_boundary_yoonlh, iterations_yoonlh, marker='o', label='YL', color='k')
nmbr_it.plot(n_boundary_juffer, iterations_juffer, marker='s', label='Juffer', color='k')

nmbr_it.set_xlabel('N of Elements')
nmbr_it.set_ylabel('GMRES Iterations')
nmbr_it.legend(loc = 0)
plt.savefig('IMG/Iterations.png')


iterations_yoonlh = [l['results']['num_cond'] for l in log 
                    if l['config']['formulation'] == 'yoon_lenhoff' 
                    and l['config']['mol_name'] == mol_name]

iterations_juffer = [l['results']['num_cond']for l in log 
                    if l['config']['formulation'] == 'juffer' 
                    and l['config']['mol_name'] == mol_name]

cond_numb = plt.figure().add_subplot(111)

cond_numb.plot(n_boundary_yoonlh, iterations_yoonlh, marker='o', label='YL', color='k')
cond_numb.plot(n_boundary_juffer, iterations_juffer, marker='s', label='Juffer', color='k')

cond_numb.set_xlabel('N of Elements')
cond_numb.set_ylabel('Condition Number')
cond_numb.legend(loc = 0)
plt.savefig('IMG/ConditionNumber.png')


