import numpy as np
import bempp.api
import json
import os


def grid_maker(face_file, vert_file):
    min_area = 1e-5
    vertx_file = open(vert_file, 'r').read().split('\n')
    faces_file = open(face_file, 'r').read().split('\n')
    
    # Counters for small triangles, and total ignored area
    xcount, atotal, a_excl = 0, 0., 0.
    vertex = np.empty((0,3))

    # Create the grid with the factory method
    factory = bempp.api.grid.GridFactory()
    
    for line in vertx_file:
        line = line.split()
        
        if len(line) != 9: 
            continue
        
        vertex = np.vstack(( vertex, np.array([line[0:3]]).astype(float) ))
        factory.insert_vertex(vertex[-1])

    # Grid assemble, exclude elements < min_area
    for line in faces_file:
        line = line.split()
        
        if len(line) != 5: 
            continue
        
        A, B, C, _, _ = np.array(line).astype(int)
        side1, side2  = vertex[B-1] - vertex[A-1], vertex[C-1] - vertex[A-1]
        face_area = 0.5*np.linalg.norm(np.cross(side1, side2))
        atotal += face_area
        
        if face_area > min_area:
            factory.insert_element([A-1, B-1, C-1])
        else:
            xcount += 1          # excluded element counter not used
            a_excl += face_area  # total area excluded not used

    grid = factory.finalize()
        
    #bempp.api.export(grid=grid, file_name=mesh_name + '.msh')
    
    return grid



def read_pqr(file):
    with open(file, 'r') as pqr_file:
            text = pqr_file.read().split('\n')

    x_q = np.empty((0,3))
    q = np.array([])

    for line in text:
            if 'ATOM' in line:
                    line = line.split()
                    coordinates = np.array(line[6:9]).astype(float)
                    charge = line[9]

                    x_q = np.vstack(( x_q, coordinates ))
                    q = np.append(q, float(charge))

    return q, x_q



def write_log(new_run):
    log_file = 'log.json'

    if not os.path.exists(log_file):
        new_file = open(log_file, 'w')
        new_file.write('[]')
        new_file.close()

    elif len(open('log.json').read())<1:
        new_file = open(log_file, 'a')  
        new_file.write('[]')
        new_file.close()

    with open('log.json') as infile:
        log = json.load(infile)

    #new_run = {"mesh_density": 1.0, "mol_name": "pelota", "total_time": 10e20, "formulation": 'asdf'}
    overwrited = False

    for i in range(len(log)):
        if log[i]["config"] == new_run["config"] \
        and log[i]["parameters_bempp"] == new_run["parameters_bempp"]:
            log[i] = new_run
            overwrited = True

    if not overwrited:
        log.append(new_run)

    log = sorted(log, key=lambda run: run['config']['mesh_density'])
    log = sorted(log, key=lambda run: run['config']['mol_name'])
    log = sorted(log, key=lambda run: run['config']['formulation'])

    with open('log.json', 'w') as outfile:
        json.dump(log, outfile, indent=4, sort_keys=True)