#!/usr/bin/python
from os import system

def change_value(variable, value):
	with open('parameters.py', 'r') as parameters_file:
		file_text = parameters_file.read().split('\n')

	#parameters_file = open('parameters.py', 'r')
	#file_text = parameters_file.read().split('\n')
	#parameters_file.close()

	if isinstance(variable, str):
		for l in range(len(file_text)):
			if variable in file_text[l]:
				file_text[l] = "{} = {}".format(variable, value)

	if isinstance(variable, float) or isinstance(variable, int):
		for l in range(len(file_text)):
			if variable in file_text[l]:
				file_text[l] = "{} = {1:4.1f}".format(variable, value)
	
	#new_parameters_file = open('parameters.py', 'w')
	#new_parameters_file.write('\n'.join(file_text))
	#new_parameters_file.close()

	with open('parameters.py', 'w') as new_parameters_file:
		new_parameters_file.write('\n'.join(file_text))


formulations = ["'juffer'", "'yoon_lenhoff'"]

for form in formulations:
	change_value("formulation", "{}".format(form))
	for density in range(1,9): 
		change_value("mesh_density", density)

		system('./RUN')
